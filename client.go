package go_grpc

import (
	"fmt"
	"google.golang.org/grpc"
)

func GetClient(serviceName string, opts ...grpc.DialOption) (*grpc.ClientConn, error) {
	serviceConfig := NewServiceConfig(serviceName)
	grpcConfig := &GrpcConfig{
		Host: serviceConfig.Host,
		Port: serviceConfig.Port,
	}
	return NewClient(grpcConfig, opts...)
}

func NewClient(grpcConfig *GrpcConfig, opts ...grpc.DialOption) (*grpc.ClientConn, error) {
	var opt []grpc.DialOption
	opt = append(opt, opts...)
	opt = append(opt, grpc.WithInsecure())
	conn, err := grpc.Dial(fmt.Sprintf("%s:%d", grpcConfig.Host, grpcConfig.Port), opt...)
	if err != nil {
		return nil, err
	}
	return conn, nil
}
