# go_grpc

#### 介绍

grpc相关组件

#### 安装教程

`go get -u gitee.com/crack007/go_grpc`

#### 使用说明

grpc 服务配置字段

```yaml

grpc:
  服务名:
    ip: 服务IP地址
    port: 服务端口

```