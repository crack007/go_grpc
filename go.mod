module gitee.com/crack007/go_grpc

go 1.15

require (
	gitee.com/crack007/go_config v1.0.3-0.20220108030920-cba7e6177f91
	gitee.com/crack007/go_log v1.10.2-0.20220108045800-9378c91a8fe1
	google.golang.org/grpc v1.26.0
)
