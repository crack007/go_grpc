package go_grpc

import (
	"fmt"
	"gitee.com/crack007/go_config"
)

func NewServiceConfig(serviceName string) *GrpcConfig {
	return &GrpcConfig{
		Host: go_config.GetString(fmt.Sprintf("grpc.%s.ip", serviceName), "127.0.0.1"),
		Port: go_config.GetUint32(fmt.Sprintf("grpc.%s.port", serviceName), 10000),
	}
}

type GrpcConfig struct {
	Host string
	Port uint32
}
